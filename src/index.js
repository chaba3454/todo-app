import React from 'react';
import ReactDOM from 'react-dom';
import { FaTrash } from 'react-icons/fa';
import { FaCheck } from 'react-icons/fa';
import { IconContext } from "react-icons";
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.scss';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newTodo: "",        // Input value
            todos: [{       // Array of todo values
                title: "Buy Milk",      // Default value
                done: false
            }]
        }
    }

    //incorporating local storage
    componentDidMount() {
        this.hydrateStateWithLocalStorage();

        // add event listener to save state to localStorage
        // when user leaves/refreshes the page
        window.addEventListener(
            "beforeunload",
            this.saveStateToLocalStorage.bind(this)
        );
    }

    componentWillUnmount() {
        window.removeEventListener(
            "beforeunload",
            this.saveStateToLocalStorage.bind(this)
        );

        // saves if component has a chance to unmount
        this.saveStateToLocalStorage();
    }

    hydrateStateWithLocalStorage() {
        // for all items in state
        for (let key in this.state) {
            // if the key exists in localStorage
            if (localStorage.hasOwnProperty(key)) {
                // get the key's value from localStorage
                let value = localStorage.getItem(key);

                // parse the localStorage string and setState
                try {
                    value = JSON.parse(value);
                    this.setState({ [key]: value });
                } catch (e) {
                    // handle empty string
                    this.setState({ [key]: value });
                }
            }
        }
    }

    saveStateToLocalStorage() {
        // for every item in React state
        for (let key in this.state) {
            // save to localStorage
            localStorage.setItem(key, JSON.stringify(this.state[key]));
        }
    }

    newTodoChanged(event) {     // 1. Change the value of input and update its value
        this.setState({     // Update the state
            newTodo: event.target.value    // Event -event(onChange), target - where this event come from, value - value located in this element
        })
    };

    formSubmitted(event) {      // 2.Call function when the form is submitted
        event.preventDefault();     // Prevent default submit

        if(this.state.newTodo === '') {     // If input has no value it would return nothing
            console.log("No item");
            return
        }

        this.setState({     // Update the state
            newTodo: '',        // On submit input value would reset with empty value
            todos: [...this.state.todos, {      // Take all the values that are currently at the array todos[] and put them in this array
                title: this.state.newTodo,      // with new object {title} that has new value of input
                done: false
            }]
        })
    };

    toggleTodoDone(event, index){       // 3.4 Function that check checkbox true or false
        const todos = [...this.state.todos];        // Take all values and the array and copy them
        todos[index] = {...todos[index]};     // Copy the todos with index
        todos[index].done = event.target.checked;       // todos[index].done = true/false
        this.setState({     // Update the state
            todos
        })
    };

    removeTodo(index) {     // 3.5 Remove one value from the array
        const todos = [...this.state.todos];      // Take all values and the array and copy them
        todos.splice(index, 1);     // .splice() deletes element from the array. index is a starting point, (1) - means delete 1 element from array
        this.setState({     // Update the state
            todos
        })
    };

    deleteAll() {       // Remove all values from the array
        const todos = [...this.state.todos];        // // Take all values and the array and copy them
        todos.splice(0);        // Deletes all elements from the array
        this.setState({     // Update the state
            todos
        })
    };

    allDone() {     // Makes all checkboxes checked
        const todos = this.state.todos.map(todo => {        // Makes new array for every element from that array
            return {        // And will return title and done is true
                title: todo.title,
                done: true
            }
        });
        this.setState({     // Update the state
            todos
        })
    };

    render() {
        return(
            <div className={'outer-container'}>
                <div className={'row mb-4 ml-0 mr-0'}>
                    <div className={'tasks col-6'}>Tasks:</div>
                    <div className={'empty col-6'}> </div>
                </div>
                <div className={'inner-container'}>
                    <ul     // 3. Create unordered list to show todos
                    >
                        {this.state.todos.map((todo, index) => {                                                        // 3.1 Creates a new array todos[] for every array element(input value)
                            return (<li key={index}                                                                     // 3.2 Create a unique key
                                        >
                                <label className="container"><span className={todo.done ? 'done' : ''}>{                // 3.3 From the array returns .title and .title in submit event is input value. If todo.done is true assign class .done with line-through, if false usual text.
                                    todo.title}</span>
                                    <input type="checkbox" className={'checkbox'}
                                                   onChange={(event) => this.toggleTodoDone(event, index)}              // 3.4 Create a checkbox. When it changes it call function to show its true or false.
                                                   checked={todo.done}/>
                                    <span className="checkmark"></span>

                                </label>
                                            <button className={'button_delete'}
                                                    onClick={                                                           // 3.5 Create button that will delete one element from the array
                                                        () => this.removeTodo(index)}>Delete</button>
                                        </li>
                            )
                        })}
                    </ul>
                    <div className={'d-flex justify-content-between'}>
                        <button onClick={       // 4. Makes all checkboxes checked
                            () => this.allDone()} className={'button_allDone'}>
                            <IconContext.Provider value={{ color: '#F7BEC1'}}><FaCheck />
                            </IconContext.Provider>
                        </button>
                        <button onClick={       // 5. Remove all values from the array
                            () => this.deleteAll()} className={'button_deleteAll'}>
                            <IconContext.Provider value={{ color: '#9B70C9'}}><FaTrash />
                            </IconContext.Provider>
                        </button>
                    </div>
                    <form onSubmit={this.formSubmitted.bind(this)}                                                      // 2. If the form is submit it will call the function
                    >
                        <div className={'input col-12'}>
                            <input className={'input-text col-12'} onChange={(event) => this.newTodoChanged(event)}     // 1. Value = newTodo, in if this value changed it would call the function that change the value of input and update it
                                   value={this.state.newTodo} placeholder="What needs to be done?.."/>
                            <button className={'button_submit'} type="submit"> </button>

                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById("root"));
